from django import template

register = template.Library()

@register.filter(name='addclass')
def addcss(value, arg):
    if 'class' in value.field.widget.attrs:
        value.field.widget.attrs['class'] += " %s" % arg
    else:
        value.field.widget.attrs['class'] = "%s" % arg
    return value.as_widget()
