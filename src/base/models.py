
from django.core import urlresolvers
from django.contrib.contenttypes.models import ContentType
from django.db import models

# TODO
class IsNotDeletedManager(models.Manager):
    def get_queryset(self):
        return super(IsNotDeletedManager, self).get_queryset().filter(is_deleted=False)


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Vytvořeno")
    changed_at = models.DateTimeField(auto_now=True, verbose_name="Změněno")
    changed_by = models.CharField(max_length=100, default="[unknown]", verbose_name="Změnil")
    is_deleted = models.BooleanField(default=False, verbose_name='Je smazáno')

    # for admin and sys calls set default manager
    admin_objects = models.Manager()
    # for frontend filter out deleted items
    objects = IsNotDeletedManager()

    class Meta:
        abstract = True

    def __str__(self):
        return "%s%s" % (
            "DEL " if self.is_deleted else "",
            self.get_str(),
        )

    def get_str(self):
        raise NotImplementedError()

    def get_admin_url(self):
        content_type = ContentType.objects.get_for_model(self.__class__)
        return urlresolvers.reverse("admin:%s_%s_change" % (content_type.app_label, content_type.model), args=(self.id,))

    def save_by(self, username):
        self.changed_by = username
        self.save()

