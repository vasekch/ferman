from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
# from django.contrib.auth import urls as auth_urls

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'', include('account.urls', namespace='account')),
    url(r'', include('event.urls', namespace='event')),
    url(r'', include('stock.urls', namespace='stock')),
    url(r'', include('contact.urls', namespace='contact')),

)\
+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
