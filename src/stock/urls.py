from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required

from django.views.generic.list import ListView
# from rest_framework import routers

from stock import views as stock_views
from .models import Item

################################################################################

# router = routers.SimpleRouter()
# router.register(r'stock', login_required(stock_views.StockViewSet))


urlpatterns = patterns('stock.views',
    url(r'^stock/?$', login_required(ListView.as_view(
        model = Item,
        # paginate_by = 2,
        queryset = Item.objects.all(),
        context_object_name = 'items',
        template_name = 'stock/item_list.html',
        )), name="list"),

    url(r'^stock-item/add/?$', login_required(stock_views.ItemCreate.as_view()), name="item-create"),
    # url(r'^stock/(?P<pk>[0-9]+)$', login_required(event_views.EventDetail.as_view()), name="item-detail"),
    url(r'^stock-item/(?P<pk>[0-9]+)/update$', login_required(stock_views.ItemUpdate.as_view()), name="item-update"),
    url(r'^stock-item/(?P<pk>[0-9]+)/delete$', login_required(stock_views.ItemDelete.as_view()), name="item-delete"),


    # url(r'^api2/', include(router.urls)),
    url(r'^api/stock/?$', login_required(stock_views.ItemListAPIView.as_view()), name="api-stock-items"),
    url(r'^api/stock/(?P<pk>[0-9]+)/?$', login_required(stock_views.ItemRetrieveAPIView.as_view()), name="api-stock-item"),

    # url(r'^api/event/(?P<event_pk>[0-9]+)/item/?$', login_required(event_views.EventItemListCreateView.as_view()), name="api-event-item-create"),
    # url(r'^api/event/(?P<event_pk>[0-9]+)/item/(?P<pk>\d+)/?$', login_required(event_views.EventItemRetrieveUpdateDestroyView.as_view()), name="api-event-item-update")



)

