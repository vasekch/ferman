from django.contrib import admin

from base.admin import BaseAdmin, BaseInlineAdmin

from .models import Item, ItemDependency, Category

class ItemDependencyInline(BaseInlineAdmin):
    # readonly_fields = (
    #     "itemdependency__severity",
    #     "itemdependency__solution_type",
    # )
    # fields = readonly_fields
    model = ItemDependency.dependent_items.through
    verbose_name = "použitá závislost"
    verbose_name_plural = "použité závislosti"


class ItemSolutionInline(BaseInlineAdmin):
    # readonly_fields = (
    #     "itemdependency__severity",
    #     "itemdependency__solution_type",
    # )
    # fields = readonly_fields
    model = ItemDependency.solution_items.through
    verbose_name = "řešené závislost"
    verbose_name_plural = "řešené závislosti"


# class DependentItemInline(BaseInlineAdmin):
#     # readonly_fields = (
#     #     "item",
#     # )
#     model = ItemDependency.dependent_items.through
#     verbose_name = "závislá položka"
#     verbose_name_plural = "závislé položky"


# class SolutionItemInline(BaseInlineAdmin):
#     # readonly_fields = (
#     #     "item",
#     # )
#     model = ItemDependency.solution_items.through
#     verbose_name = "vyžadovaná položka"
#     verbose_name_plural = "vyžadované položky"


class CategoryAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "position"
    )
    list_editable = ("position", )


class ItemAdmin(BaseAdmin):
    # exclude = ("is_deleted", ) # will not work, when overriding get_form method
    list_display = (
        "category",
        "name",
        "stock_count",
        "dependencies_count",
        "admin_is_active",
        "created_at",
        "changed_by",
        "changed_at",
    )
    list_display_links = (
        "name",
    )
    fields = (
        'name',
        'stock_count',
        'category',
        'changed_by',
        'is_deleted',
    )
    readonly_fields = ('changed_by',)
    # list_editable =("is_deleted",)
    list_filter = ('category', 'is_deleted', )
    search_fields = ("=name", )
    # search_fields = ("=customer__name", )
    save_as = True
    inlines = (
        ItemDependencyInline,
        ItemSolutionInline,
    )
    radio_fields = {"category": admin.VERTICAL}


class ItemDependencyAdmin(BaseAdmin):
    list_display = (
        'name',
        'severity',
        'solution_type',
        'dependent_items_count',
        'solution_items_count',
    )
    list_filter = ('severity', 'solution_type')
    # exclude = ('dependent_items', 'solution_items', )
    # inlines = (
    #     DependentItemInline,
    #     SolutionItemInline,
    # )

    radio_fields = {
        "severity": admin.VERTICAL,
        "solution_type": admin.VERTICAL,
    }
    filter_horizontal = ("dependent_items", "solution_items")


admin.site.register(Category, CategoryAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(ItemDependency, ItemDependencyAdmin)
