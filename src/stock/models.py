from django.db import models
from base.models import BaseModel

ITEM_DEPENDENCY_SEVERITY_MUST = 1
ITEM_DEPENDENCY_SEVERITY_ADVISE = 2

ITEM_DEPENDENCY_SEVERITY_CHOICES = (
    (ITEM_DEPENDENCY_SEVERITY_MUST, 'nutnost'),
    (ITEM_DEPENDENCY_SEVERITY_ADVISE, 'doporučení'),
)

ITEM_DEPENDENCY_SOLUTION_ALL = 1
ITEM_DEPENDENCY_SOLUTION_ANY = 2

ITEM_DEPENDENCY_SOLUTION_CHOICES = (
    (ITEM_DEPENDENCY_SOLUTION_ALL, 'vyžadovány všechny položky'),
    (ITEM_DEPENDENCY_SOLUTION_ANY, 'vyžadována alespoň jedna položka'),
)

class Category(models.Model):
    name = models.CharField(unique=True, max_length=200, verbose_name="název")
    position = models.IntegerField(verbose_name="pořadí")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "kategorie"
        verbose_name_plural = "kategorie"
        ordering = ('position', )


class Item(BaseModel):
    category = models.ForeignKey(Category, blank=True, on_delete=models.SET_NULL, null=True, verbose_name="kategorie")
    name = models.CharField(unique=True, max_length=200, verbose_name="pracovní název")
    # proposal_name = models.CharField(blank=True, verbose_name="název do nabídky")
    # proposal_default = models.BooleanField
    stock_count = models.SmallIntegerField(default=0, verbose_name="počet kusů celkem")
    # weight = models.FloatField
    # picture = models.ImageField
    # description = models.TextField

    def get_str(self):
        if self.category: return "%s — %s" % (self.category.name, self.name)
        return self.name

    def dependencies_count(self):
        return self.dependencies.all().count()
    dependencies_count.short_description = "počet závislostí"

    class Meta:
        verbose_name = "položka materiálu"
        verbose_name_plural = "materiál"
        ordering = ('category__position', 'name',)


class ItemDependency(BaseModel):
    name = models.CharField(unique=True, max_length=200, verbose_name="název")
    severity = models.PositiveSmallIntegerField(choices=ITEM_DEPENDENCY_SEVERITY_CHOICES, default=ITEM_DEPENDENCY_SEVERITY_MUST, verbose_name="důležitost")
    # description = models.CharField(max_length=500, blank=True, verbose_name="popis")
    dependent_items = models.ManyToManyField(Item, blank=True, related_name='dependencies', verbose_name="použít pro položky")
    solution_type = models.PositiveSmallIntegerField(choices=ITEM_DEPENDENCY_SOLUTION_CHOICES, default=ITEM_DEPENDENCY_SOLUTION_ALL, verbose_name="typ řešení", help_text="Co je třeba splnit k vyřešení závislosti.")
    solution_items = models.ManyToManyField(Item, blank=True, related_name='solved_dependencies', verbose_name="vyžadované položky")

    def __str__(self):
        return self.name

    def dependent_items_count(self):
        return self.dependent_items.all().count()
    dependent_items_count.short_description = "počet použití"

    def solution_items_count(self):
        return self.solution_items.all().count()
    solution_items_count.short_description = "počet řešení"


    class Meta:
        verbose_name = "závislost položky"
        verbose_name_plural = "závislosti položek"
        ordering = ('severity', 'name')
