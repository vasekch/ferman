# from django.shortcuts import render, redirect
# from django.views.generic import TemplateView #, View #, DetailView, View
# from django.views.generic.edit import UpdateView
from rest_framework import generics
# from rest_framework import permissions, viewsets
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from .forms import ItemForm
from .models import Item
from .serializers import ItemSerializer


# class StockView(TemplateView):
#     template_name = "list.html"

#     def get_context_data(self, **kwargs):
#         kwargs = super(StockView, self).get_context_data(**kwargs)

#         kwargs.update({
#             "items": Item.objects.all(),
#         })

#         return kwargs





class ItemDelete(DeleteView):
    model = Item
    success_url = reverse_lazy('stock:list')

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.is_deleted = True
        self.object.save_by(request.user.username)
        return HttpResponseRedirect(success_url)


class ItemCreate(CreateView):
    model = Item
    form_class = ItemForm
    success_url = reverse_lazy('stock:list')

    def form_valid(self, form):
        form.instance.changed_by = self.request.user.username
        return super(ItemCreate, self).form_valid(form)


class ItemUpdate(UpdateView):
    model = Item
    form_class = ItemForm
    success_url = reverse_lazy('stock:list')


    def form_valid(self, form):
        form.instance.changed_by = self.request.user.username
        return super(ItemUpdate, self).form_valid(form)







class ItemListAPIView(generics.ListAPIView):
    serializer_class = ItemSerializer

    def get_queryset(self):
        return Item.objects.all()


class ItemRetrieveAPIView(generics.RetrieveAPIView):
    serializer_class = ItemSerializer

    def get_queryset(self):
        return Item.objects.filter(id=self.kwargs['pk'])


# class StockViewSet(viewsets.ModelViewSet):
#     queryset = Item.objects.all()
#     serializer_class = ItemSerializer

#     def get_permissions(self):
#         return (permissions.AllowAny(), )
#         # if self.request.method in permissions.SAFE_METHODS:
#         #     return (permissions.AllowAny(), )
#         # return (permissions.IsAuthenticated(), IsAuthorOfPost(), )

#     # def perform_create(self, serializer):
#     #     instance = serializer.save(author=self.request.user)

#     #     return super(PostViewSet, self).perform_create(serializer)
