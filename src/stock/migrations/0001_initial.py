# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, unique=True, verbose_name='název')),
                ('position', models.IntegerField(verbose_name='pořadí')),
            ],
            options={
                'verbose_name_plural': 'kategorie',
                'verbose_name': 'kategorie',
                'ordering': ('position',),
            },
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Vytvořeno')),
                ('changed_at', models.DateTimeField(auto_now=True, verbose_name='Změněno')),
                ('changed_by', models.CharField(max_length=100, default='[unknown]', verbose_name='Změnil')),
                ('is_deleted', models.BooleanField(default=False, verbose_name='Je smazáno')),
                ('name', models.CharField(max_length=200, unique=True, verbose_name='pracovní název')),
                ('stock_count', models.SmallIntegerField(default=0, verbose_name='počet kusů celkem')),
                ('category', models.ForeignKey(blank=True, to='stock.Category', on_delete=django.db.models.deletion.SET_NULL, verbose_name='kategorie', null=True)),
            ],
            options={
                'verbose_name_plural': 'materiál',
                'verbose_name': 'položka materiálu',
                'ordering': ('category__position', 'name'),
            },
        ),
        migrations.CreateModel(
            name='ItemDependency',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Vytvořeno')),
                ('changed_at', models.DateTimeField(auto_now=True, verbose_name='Změněno')),
                ('changed_by', models.CharField(max_length=100, default='[unknown]', verbose_name='Změnil')),
                ('is_deleted', models.BooleanField(default=False, verbose_name='Je smazáno')),
                ('name', models.CharField(max_length=200, unique=True, verbose_name='název')),
                ('severity', models.PositiveSmallIntegerField(choices=[(1, 'nutnost'), (2, 'doporučení')], default=1, verbose_name='důležitost')),
                ('solution_type', models.PositiveSmallIntegerField(choices=[(1, 'vyžadovány všechny položky'), (2, 'vyžadována alespoň jedna položka')], default=1, help_text='Co je třeba splnit k vyřešení závislosti.', verbose_name='typ řešení')),
                ('dependent_items', models.ManyToManyField(blank=True, to='stock.Item', verbose_name='použít pro položky', related_name='dependencies')),
                ('solution_items', models.ManyToManyField(blank=True, to='stock.Item', verbose_name='vyžadované položky', related_name='solved_dependencies')),
            ],
            options={
                'verbose_name_plural': 'závislosti položek',
                'verbose_name': 'závislost položky',
                'ordering': ('severity', 'name'),
            },
        ),
    ]
