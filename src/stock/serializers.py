from rest_framework import serializers

from .models import Category, Item

class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ('id', 'name')
        read_only_fields = fields


class ItemSerializer(serializers.ModelSerializer):
    category = CategorySerializer(read_only=True)

    class Meta:
        model = Item
        fields = ('id', 'category', 'name', 'stock_count', 'created_at', 'changed_at', 'changed_by')
        read_only_fields = ('id', 'created_at', 'changed_at', 'changed_by')

