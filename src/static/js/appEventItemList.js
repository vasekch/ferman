'use strict'

angular.module('eventItems', ['ngResource', 'ui.select', 'ngSanitize'])

.config(function($httpProvider) {
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
})


.service('eventItemService', function($resource) {
    return $resource(
        '/api/event/:event_id/item/:event_item_id',
        {event_id: event_id, event_item_id: '@id'},
        {
            save: {method: 'POST', isArray:false},
            query: {method: 'GET', isArray:true},
            update: {method: 'PUT', isArray:false},
            destroy: {method: 'DELETE', isArray:false}
        }
    );

})



.service('itemService', function($resource) {
    return $resource(
        '/api/stock/:item_id',
        {item_id: '@id'},
        {
            // save: {method: 'POST', isArray:false},
            query: {method: 'GET', isArray:true},
            // update: {method: 'PUT', isArray:false},
            // destroy: {method: 'DELETE', isArray:false}
        }
    );

})


.controller('EventItemListController', function(eventItemService, itemService) {

    var eventItemList = this;
    eventItemList.event_items = [];
    eventItemList.item_options = [];

    // eventItemList.new_event_id = 0;
    eventItemList.new_item_id = null;
    eventItemList.new_requested_count = 1;

    eventItemService.query(function(response){
        eventItemList.event_items = response;
    });

    itemService.query(function(response){
        eventItemList.item_options = response;
    });


    eventItemList.group_by_category = function(item){
        if (item.category) {
            return item.category.name;
        }
    }

    eventItemList.createEventItem = function () {
        var event_item = new eventItemService();
        // event_item.event = event_id;
        // console.log(eventItemList);
        // event_item.event_id = eventItemList.new_event_id;
        event_item.item_id = eventItemList.new_item_id;
        event_item.requested_count = eventItemList.new_requested_count;
        event_item.$save(function(response){
            eventItemList.event_items.push(response);
            eventItemList.new_item_id = null;
            eventItemList.new_requested_count = 1;
        });
    };


    eventItemList.changeEventItem = function(event_item, delta) {
        if (event_item.requested_count + delta <=0) {
            if (confirm("Opravdu chcete položku smazat?")) {
                event_item.$delete(function(response){
                    // eventItemService.destroy({'id': event_item.id, 'requested_count': event_item.requested_count});
                    var index = eventItemList.event_items.indexOf(event_item);
                    eventItemList.event_items.splice(index, 1);
                });
            }
        }
        else {
            event_item.requested_count += delta;
            eventItemService.update({'id': event_item.id, 'requested_count': event_item.requested_count});
        }
    };

});


