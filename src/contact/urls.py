from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required

from django.views.generic.list import ListView

from contact import views as contact_views
from .models import Contact

################################################################################


urlpatterns = patterns('contact.views',
    # url(r'^contacts$', login_required(contact_views.ContactView.as_view()), name="list"),

    url(r'^contact/?$', login_required(ListView.as_view(
        model = Contact,
        # paginate_by = 2,
        queryset = Contact.objects.all(),
        context_object_name = 'contacts',
        template_name = 'contact/contact_list.html',
        )), name="list"),

    url(r'^contact/add/?$', login_required(contact_views.ContactCreate.as_view()), name="contact-create"),
    # url(r'^contact/(?P<pk>[0-9]+)$', login_required(event_views.EventDetail.as_view()), name="contact-detail"),
    url(r'^contact/(?P<pk>[0-9]+)/update$', login_required(contact_views.ContactUpdate.as_view()), name="contact-update"),
    url(r'^contact/(?P<pk>[0-9]+)/delete$', login_required(contact_views.ContactDelete.as_view()), name="contact-delete"),


)
