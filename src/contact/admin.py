from django.contrib import admin

from base.admin import BaseAdmin, BaseInlineAdmin

from .models import Contact, Skill


class SkillAdmin(admin.ModelAdmin):
    # exclude = ("is_deleted", ) # will not work, when overriding get_form method
    list_display = (
        "name",
        "position",
    )
    # list_display_links = (
    #     "get_environment",
    # )
    list_editable =("position",)


class ContactAdmin(BaseAdmin):
    # exclude = ("is_deleted", ) # will not work, when overriding get_form method
    list_display = (
        "name",
        "phone",
        "email",
        "account",
        "get_skills_text",
        "get_remark_text",
        "is_customer",
        "admin_is_active",
        "created_at",
        "changed_by",
        "changed_at",
    )
    # list_display_links = (
    #     "get_environment",
    # )
    # list_editable =("name", "phone", "email",)
    list_filter = ('is_customer', 'skills')
    search_fields = ("=name", "=phone", "=email")
    filter_horizontal = ("skills",)



admin.site.register(Skill, SkillAdmin)
admin.site.register(Contact, ContactAdmin)
