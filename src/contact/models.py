from django.db import models
from django.conf import settings

from base.models import BaseModel


class Skill(models.Model):
    name = models.CharField(unique=True, max_length=200, verbose_name="název")
    position = models.IntegerField(verbose_name="pořadí")

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('position', )
        verbose_name = "dovednost kontaktu"
        verbose_name_plural = "dovednosti kontaktu"


class Contact(BaseModel):
    name = models.CharField(unique=True, max_length=200, verbose_name="jméno")
    is_customer = models.BooleanField(default=False, blank=True, verbose_name="zákazník")
    phone = models.CharField(blank=True, max_length=200, verbose_name="telefon")
    email = models.EmailField(blank=True, verbose_name="email")
    remark = models.TextField(blank=True, verbose_name="poznámka")
    account = models.OneToOneField(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.SET_NULL, verbose_name="login")
    # company = models.ForeignKey(Company, blank="true", null="true")
    skills = models.ManyToManyField(Skill, blank=True, verbose_name="dovednosti")

    def get_str(self):
        return self.name

    def get_skills_text(self):
        return ", ".join([skill.name for skill in self.skills.all()])
    get_skills_text.short_description = "dovednosti"

    def get_remark_text(self):
        diplayed_chars = 80
        ending = ""
        if len(self.remark) > diplayed_chars:
            ending = "…"
        return "%s%s" % (self.remark[:diplayed_chars], ending)
    get_remark_text.short_description = "poznámka"

    class Meta:
        ordering = ('name', )
        verbose_name = "kontakt"
        verbose_name_plural = "kontakty"

