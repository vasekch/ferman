# from django.shortcuts import render, redirect
# from django.views.generic import TemplateView #, View #, DetailView, View
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.views.generic.edit import CreateView, UpdateView, DeleteView
# from django.views.generic.edit import UpdateView

from .forms import ContactForm
from .models import Contact



# class ContactView(TemplateView):
#     template_name = "list.html"

#     def get_context_data(self, **kwargs):
#         kwargs = super(ContactView, self).get_context_data(**kwargs)

#         kwargs.update({
#             "contacts": Contact.objects.all(),
#         })

#         return kwargs


class ContactDelete(DeleteView):
    model = Contact
    success_url = reverse_lazy('contact:list')

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.is_deleted = True
        self.object.save_by(request.user.username)
        return HttpResponseRedirect(success_url)


class ContactCreate(CreateView):
    model = Contact
    form_class = ContactForm
    success_url = reverse_lazy('contact:list')

    def form_valid(self, form):
        form.instance.changed_by = self.request.user.username
        return super(ContactCreate, self).form_valid(form)


class ContactUpdate(UpdateView):
    model = Contact
    form_class = ContactForm
    success_url = reverse_lazy('contact:list')


    def form_valid(self, form):
        form.instance.changed_by = self.request.user.username
        return super(ContactUpdate, self).form_valid(form)


