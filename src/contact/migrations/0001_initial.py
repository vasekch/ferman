# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Vytvořeno')),
                ('changed_at', models.DateTimeField(auto_now=True, verbose_name='Změněno')),
                ('changed_by', models.CharField(max_length=100, default='[unknown]', verbose_name='Změnil')),
                ('is_deleted', models.BooleanField(default=False, verbose_name='Je smazáno')),
                ('name', models.CharField(max_length=200, unique=True, verbose_name='jméno')),
                ('is_customer', models.BooleanField(default=False, verbose_name='zákazník')),
                ('phone', models.CharField(blank=True, max_length=200, verbose_name='telefon')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='email')),
                ('remark', models.TextField(blank=True, verbose_name='poznámka')),
                ('account', models.OneToOneField(blank=True, to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.SET_NULL, verbose_name='login', null=True)),
            ],
            options={
                'verbose_name_plural': 'kontakty',
                'verbose_name': 'kontakt',
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='Skill',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, unique=True, verbose_name='název')),
                ('position', models.IntegerField(verbose_name='pořadí')),
            ],
            options={
                'verbose_name_plural': 'dovednosti kontaktu',
                'verbose_name': 'dovednost kontaktu',
                'ordering': ('position',),
            },
        ),
        migrations.AddField(
            model_name='contact',
            name='skills',
            field=models.ManyToManyField(blank=True, to='contact.Skill', verbose_name='dovednosti'),
        ),
    ]
