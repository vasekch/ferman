from django.core.urlresolvers import reverse
from django.db import models

from base.models import BaseModel

from contact.models import Contact
from stock.models import Item, ITEM_DEPENDENCY_SOLUTION_ALL, ITEM_DEPENDENCY_SEVERITY_MUST


class Event(BaseModel):
    name = models.CharField(max_length=200, verbose_name="název akce")
    start = models.DateField(verbose_name="začátek")
    end = models.DateField(verbose_name="konec")
    customer = models.ForeignKey(Contact, blank=True, null=True, on_delete=models.SET_NULL, limit_choices_to={'is_customer': True}, verbose_name="zákazník")
    manager = models.ForeignKey(Contact, blank=True, null=True, related_name='managed_events', on_delete=models.SET_NULL, limit_choices_to={'account__isnull': False}, verbose_name="zodpovídá")
    remark = models.TextField(blank=True, verbose_name="poznámka")

    items = models.ManyToManyField(Item, through='EventItem', related_name='events', verbose_name="materiál")



    def get_str(self):
        return "%s %s" % (self.name, self.start)

    def length(self):
        return (self.end - self.start).days + 1

    def get_absolute_url(self):
            return reverse('event:event-detail', args=(self.id,))

    def get_dependencies(self):
        dependency_set = set()
        self.dependencies = list()
        # items = self.items.all().prefetch_related('solution_items.')
        items = self.items.all().prefetch_related('eventitem_set__item__dependencies__solution_items')#.filter(solution_items__is_deleted=False)
        for item in items:
            for dependency in item.dependencies.filter(is_deleted=False):
                dependency_set.add(dependency)


        for dependency in dependency_set:
            solution_items = list()
            dependent_items = list()

            if dependency.solution_type == ITEM_DEPENDENCY_SOLUTION_ALL:
                solved_requested = True
                solved_booked = True
            else:
                solved_requested = False
                solved_booked = False

            rc_src_sum = 0
            bc_src_sum = 0

            for source_item in dependency.dependent_items.all():
                if source_item in items:
                    event_item = self.eventitem_set.get(item=source_item)
                    rc_src_sum += event_item.requested_count
                    bc_src_sum += event_item.booked_count

            rc_sol_sum = 0
            bc_sol_sum = 0

            for solution_item in dependency.solution_items.all():
                rc = 0
                bc = 0
                if solution_item in items:
                    event_item = self.eventitem_set.get(item=solution_item)
                    rc = event_item.requested_count
                    bc = event_item.booked_count
                solution_items.append({'item': solution_item, 'requested_count': rc, 'booked_count': bc})
                rc_sol_sum += rc
                bc_sol_sum += bc

                if dependency.solution_type == ITEM_DEPENDENCY_SOLUTION_ALL:
                    # any missing solution item breaks dependency
                    if rc <= 0:
                        solved_requested = False
                    if bc <= 0 or bc < rc:
                        solved_booked = False
                else:
                    # any solution item fulfills dependency
                    if rc > 0:
                        solved_requested = True
                    if bc > 0 and bc == rc:
                        solved_booked = True


            for dependent_item in dependency.dependent_items.all():
                if dependent_item in items:
                    event_item = self.eventitem_set.get(item = dependent_item)
                    dependent_items.append({'item': dependent_item, 'requested_count': event_item.requested_count, 'booked_count': event_item.booked_count})


            self.dependencies.append({
                'dependency': dependency,
                'dependent_items': dependent_items,
                'solution_items': solution_items,
                'must': bool(dependency.severity == ITEM_DEPENDENCY_SEVERITY_MUST),
                'requested_solved': solved_requested,
                'requested_source_count': rc_src_sum,
                'requested_solution_count': rc_sol_sum,
                'booked_solved': solved_booked,
                'booked_source_count': bc_src_sum,
                'booked_solution_count': bc_sol_sum,
            })
        return self.dependencies


    class Meta:
        ordering = ('start', )
        verbose_name = "akce"
        verbose_name_plural = "akce"


class EventItem(models.Model):
    event = models.ForeignKey(Event, verbose_name="akce")
    item = models.ForeignKey(Item, verbose_name="položka")
    requested_count = models.SmallIntegerField(default="1", verbose_name="požadovaný počet")
    booked_count = models.SmallIntegerField(default="0", verbose_name="rezervovaný počet")

    class Meta:
        ordering = ('item__category__position', 'item__name')
        verbose_name = "materiál na akci"
        verbose_name_plural = "materiál na akce"
        unique_together = (('event', 'item'), )

    def save(self, **kwargs):
        # prevent booking of more than requested items (when manipulating with requested number)
        if int(self.booked_count) > self.requested_count:
            self.booked_count = self.requested_count
        super(EventItem, self).save(**kwargs)

    @property
    def available_count(self):
        # available and not booked items for all event duration
        # TODO
        qs = EventItem.objects.filter(item=self.item, event__start__lte=self.event.end, event__end__gte=self.event.start).aggregate(models.Sum('booked_count'))
        return self.item.stock_count - qs['booked_count__sum']

    @property
    def to_be_booked_count(self):
        # not yet booked, but requested items
        tbb_count = self.requested_count - self.booked_count
        if tbb_count > 0:
            return tbb_count
        return 0

    @property
    def to_be_booked_by_others_count(self):
        # not yet booked, but requested items
        rqs = EventItem.objects.filter(item=self.item, event__start__lte=self.event.end, event__end__gte=self.event.start).aggregate(models.Sum('requested_count'))
        bqs = EventItem.objects.filter(item=self.item, event__start__lte=self.event.end, event__end__gte=self.event.start).aggregate(models.Sum('booked_count'))
        tbbo_count = rqs['requested_count__sum'] - bqs['booked_count__sum'] - self.requested_count
        if tbbo_count > 0:
            return tbbo_count
        return 0

    @property
    def missing_count(self):
        # not yet booked, but requested items
        if self.requested_count > self.booked_count:
            missing_count = self.requested_count - self.booked_count - self.available_count
            if missing_count > 0:
                return missing_count
        return 0
