from datetime import datetime
from rest_framework import serializers

# from stock.models import Item
from stock.serializers import ItemSerializer
# from .models import Event, EventItem
from .models import EventItem

# class EventSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Event
#         fields = ('id', 'name', 'start', 'end', 'customer', 'manager', 'remark', 'created_at', 'changed_at', 'changed_by')
#         read_only_fields = ('id', 'created_at', 'changed_at', 'changed_by')




class EventItemSimpleSerializer(serializers.ModelSerializer):

    class Meta:
        model = EventItem

        fields = ('id', 'requested_count', )
        read_only_fields = ('id',  )

    def update(self, instance, validated_data):
        instance.event.changed_at = datetime.now()
        instance.event.changed_by = self.context['request'].user.username
        instance.event.save()
        return super(EventItemSimpleSerializer, self).update(instance, validated_data)


class EventItemSerializer(serializers.ModelSerializer):
    item_name = serializers.CharField(read_only=True, source='item.name')

    class Meta:
        model = EventItem

        fields = ('id', 'event', 'item', 'item_name', 'requested_count', 'booked_count')
        read_only_fields = ('id', 'item_name', 'booked_count', )


    def create(self, validated_data):
        validated_data['event'].changed_at = datetime.now()
        validated_data['event'].changed_by = self.context['request'].user.username
        validated_data['event'].save()
        return super(EventItemSerializer, self).create(validated_data)

