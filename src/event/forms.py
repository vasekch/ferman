
from django import forms

from .models import Event

class EventForm(forms.ModelForm):

    class Meta:
        model = Event
        fields = [
            'name',
            'customer',
            'manager',
            'start',
            'end',
            'remark',
        ]


    def clean(self):
        cleaned_data = super(EventForm, self).clean()
        start_date = self.cleaned_data.get("start")
        end_date = self.cleaned_data.get("end")
        if start_date and end_date and end_date < start_date:
            raise forms.ValidationError("Start akce musí být dřív, než konec")
        return cleaned_data
