from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required

from event import views as event_views

################################################################################


urlpatterns = patterns('event.views',
    url(r'^$', login_required(event_views.DashboardView.as_view()), name="dashboard"),
    url(r'^set-dashboard-start/(?P<date_arg>\d{4}-\d{2}-\d{2})$', login_required(event_views.setDashboardStart), name="set-dashboard-start"),
    url(r'^set-dashboard-end/(?P<date_arg>\d{4}-\d{2}-\d{2})$', login_required(event_views.setDashboardEnd), name="set-dashboard-end"),
    url(r'^reset-dashboard-dates$', login_required(event_views.resetDashboardDates), name="reset-dashboard-dates"),
    url(r'^event-add/(?P<start>\d{4}-\d{2}-\d{2})?$', login_required(event_views.EventCreate.as_view()), name="event-create"),
    # url(r'^event/(?P<pk>[0-9]+)$', login_required(event_views.eventDetail), name="event-detail"),
    url(r'^event/(?P<pk>[0-9]+)$', login_required(event_views.EventDetail.as_view()), name="event-detail"),
    url(r'^event/(?P<pk>[0-9]+)/update$', login_required(event_views.EventUpdate.as_view()), name="event-update"),
    url(r'^event/(?P<pk>[0-9]+)/delete$', login_required(event_views.EventDelete.as_view()), name="event-delete"),
    url(r'^event/(?P<pk>[0-9]+)/items$', login_required(event_views.EventItemList.as_view()), name="event-items"),
    url(r'^event/(?P<pk>[0-9]+)/book-all-possible$', login_required(event_views.bookAllPossible), name="book-all-possible"),

    # url(r'^event/(?P<pk>[0-9]+)/items$', login_required(event_views.EventItemList.as_view()), name="event-items"),
    # url(r'^event/(?P<pk>[0-9]+)/items$', login_required(event_views.EventItemList.as_view()), name="event-items"),

    # url(r'^event/(?P<pk>[0-9]+)/items/api/event/?$', event_views.EventItemListCreateView.as_view(), name="event-item-list-create"),
    # url(r'^event/(?P<pk>[0-9]+)/items/api/event/(?P<pk>\d+)/?$', event_views.EventItemView.as_view(), name="event-item-list-create")

    url(r'^api/event/(?P<event_pk>[0-9]+)/item/?$', login_required(event_views.EventItemListCreateView.as_view()), name="api-event-item-create"),
    url(r'^api/event/(?P<event_pk>[0-9]+)/item/(?P<pk>\d+)/?$', login_required(event_views.EventItemRetrieveUpdateDestroyView.as_view()), name="api-event-item-update")

)



