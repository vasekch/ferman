# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('stock', '0001_initial'),
        ('contact', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Vytvořeno')),
                ('changed_at', models.DateTimeField(auto_now=True, verbose_name='Změněno')),
                ('changed_by', models.CharField(max_length=100, default='[unknown]', verbose_name='Změnil')),
                ('is_deleted', models.BooleanField(default=False, verbose_name='Je smazáno')),
                ('name', models.CharField(max_length=200, verbose_name='název akce')),
                ('start', models.DateField(verbose_name='začátek')),
                ('end', models.DateField(verbose_name='konec')),
                ('remark', models.TextField(blank=True, verbose_name='poznámka')),
                ('customer', models.ForeignKey(blank=True, to='contact.Contact', on_delete=django.db.models.deletion.SET_NULL, verbose_name='zákazník', null=True)),
            ],
            options={
                'verbose_name_plural': 'akce',
                'verbose_name': 'akce',
                'ordering': ('start',),
            },
        ),
        migrations.CreateModel(
            name='EventItem',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('requested_count', models.SmallIntegerField(default='1', verbose_name='požadovaný počet')),
                ('booked_count', models.SmallIntegerField(default='0', verbose_name='rezervovaný počet')),
                ('event', models.ForeignKey(to='event.Event', verbose_name='akce')),
                ('item', models.ForeignKey(to='stock.Item', verbose_name='položka')),
            ],
            options={
                'verbose_name_plural': 'materiál na akce',
                'verbose_name': 'materiál na akci',
                'ordering': ('item__category__position', 'item__name'),
            },
        ),
        migrations.AddField(
            model_name='event',
            name='items',
            field=models.ManyToManyField(to='stock.Item', through='event.EventItem', verbose_name='materiál', related_name='events'),
        ),
        migrations.AddField(
            model_name='event',
            name='manager',
            field=models.ForeignKey(blank=True, to='contact.Contact', related_name='managed_events', on_delete=django.db.models.deletion.SET_NULL, verbose_name='zodpovídá', null=True),
        ),
        migrations.AlterUniqueTogether(
            name='eventitem',
            unique_together=set([('event', 'item')]),
        ),
    ]
