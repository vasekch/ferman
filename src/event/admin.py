from django.contrib import admin

from base.admin import BaseAdmin, BaseInlineAdmin

from .models import Event, EventItem

class EventItemInline(BaseInlineAdmin):
    model = EventItem

class EventAdmin(BaseAdmin):
    # exclude = ("is_deleted", ) # will not work, when overriding get_form method
    list_display = (
        "name",
        # "get_environment",
        "start",
        "end",
        # "get_testsetups_count",
        # "get_agents_count",

        # "admin_is_active",
        # "position",
        "admin_is_active",
        "created_at",
        "changed_by",
        "changed_at",
    )
    # list_display_links = (
    #     "get_environment",
    # )
    # list_editable =("is_deleted",)

    readonly_fields = ['created_at', 'changed_by', 'changed_at']

    list_filter = ("is_deleted", )
    search_fields = ("=name", )
    # search_fields = ("=customer__name", )
    save_as = True
    inlines = (
        EventItemInline,
    )


admin.site.register(Event, EventAdmin)
