from datetime import date, timedelta
import dateutil.parser

from rest_framework import generics, status
from rest_framework.response import Response

from django.conf import settings
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import DetailView, ListView, TemplateView  # View
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from stock.models import Item

from .models import Event, EventItem
from .forms import EventForm
from .serializers import EventItemSerializer, EventItemSimpleSerializer



def setDashboardStart(request, date_arg):
    request.session['dashboard_start'] = date_arg
    return redirect('event:dashboard')


def setDashboardEnd(request, date_arg):
    request.session['dashboard_end'] = date_arg
    return redirect('event:dashboard')


def resetDashboardDates(request):
    if request.session.get('dashboard_start'): del request.session['dashboard_start']
    if request.session.get('dashboard_end'): del request.session['dashboard_end']
    return redirect('event:dashboard')


def bookAllPossible(request, pk):
    event = get_object_or_404(Event, pk=pk)
    total_booked_count = 0
    for eventitem in event.eventitem_set.all():
        if eventitem.to_be_booked_count > 0:
            if eventitem.available_count > 0:
                eventitem.booked_count = eventitem.available_count
                eventitem.save()
                messages.success(request, "Zamluveno: %s %d×" % (eventitem.item.name, eventitem.booked_count))
                total_booked_count += eventitem.booked_count
    if total_booked_count == 0:
        messages.warning(request, "Nebylo možné zarezervovat žádnou položku")
    return redirect('event:event-detail', pk=pk)



class DashboardView(TemplateView):
    template_name = "dashboard.html"

    def get_context_data(self, **kwargs):
        kwargs = super(DashboardView, self).get_context_data(**kwargs)

        # get from/to from session or set default
        show_reset_dates = False
        dashboard_start = self.request.session.get("dashboard_start", None)
        if dashboard_start is None:
            dashboard_start = date.today() - timedelta(days=settings.DASHBOARD_DEFAULT_DAYS_HISTORY)
        else:
            dashboard_start = dateutil.parser.parse(dashboard_start).date()
            show_reset_dates = True

        dashboard_end = self.request.session.get("dashboard_end", None)
        if dashboard_end is None:
            dashboard_end = date.today() + timedelta(days=settings.DASHBOARD_DEFAULT_DAYS_FUTURE)
        else:
            dashboard_end = dateutil.parser.parse(dashboard_end).date()
            show_reset_dates = True


        events_queryset = Event.objects.filter(start__lt=dashboard_end, end__gt=dashboard_start).order_by('start', '-end')#.select_related('manager')
        # use events as stack
        events = list(events_queryset)


        # generate rows of calendar
        cal = []

        d = dashboard_start
        day_strings = ['Po', 'Út', 'St', 'Čt', 'Pá', 'So', 'Ne']
        weekdays = set([5, 6])
        while d <= dashboard_end:
            cal.append({'date': d, 'day': day_strings[d.weekday()], 'weekend': d.weekday() in weekdays, 'cols':[]})
            d += timedelta(days=1)

        total_rows = (dashboard_end - dashboard_start).days + 1

        # assign events to columns
        # assign columns to rows, each event has proper rowspan
        c = 1 # col pointer starts with second column, first contains dates
        while len(events):
            next_events = [] # helper list for event that could not be assignet do current column
            r = 0 # row pointer

            try:

                while r < total_rows:
                    rundown = False
                    runoff = False
                    e = events.pop(0)
                    # event started before cal
                    if e.start < dashboard_start:
                        e.start = dashboard_start
                        rundown = True
                    # event ends after cal
                    if e.end > dashboard_end:
                        e.end = dashboard_end
                        runoff = True
                    # if can not use this column for this event, events are over lapsing
                    if e.start < cal[r]['date']:
                        # postpone it to next column
                        next_events.append(e)
                        # start over to pick the next one
                        continue
                    # skip rows until row date and event start date are equal
                    while cal[r]['date'] < e.start:
                        cal[r]['cols'].append({'empty': True})
                        r += 1

                    # cal[r]['date'] == e.start at this point if data not corrupted
                    # assign event to col
                    cal[r]['cols'].append({'event': e, 'rundown': rundown, 'runoff': runoff, 'rowspan': e.length})
                    r += 1
                    # skip rows according to event length
                    while cal[r]['date'] <= e.end:
                        cal[r]['cols'].append(None)
                        r += 1

                # runned out all rows, append any leftover of events to next_events
                next_events += events

            except IndexError:
                # events is empty
                # pass
                # fill rest of the rows by empty values
                while r < total_rows and cal[r]['date'] <= dashboard_end:
                    cal[r]['cols'].append({'empty': True})
                    r += 1

            finally:
                # try use leftovers in next_events
                events = next_events

            # proceed to the next column
            c += 1

        kwargs.update({
            "events": events_queryset,
            "cal": cal,
            "today": date.today(),
            "dashboard_start": dashboard_start,
            "dashboard_end": dashboard_end,
            "show_reset_dates": show_reset_dates,
        })

        return kwargs


# def eventDetail(request, pk):
#     event = Event.objects.get(id=pk)

#     return render(request, 'event/event_detail.html', {
#         'event': event
#     })

class EventDetail(DetailView):
    queryset = Event.objects.all().prefetch_related('eventitem_set__item__dependencies').select_related('customer', 'manager')


class EventDelete(DeleteView):
    model = Event
    success_url = reverse_lazy('event:dashboard')

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.is_deleted = True
        self.object.save_by(request.user.username)
        return HttpResponseRedirect(success_url)


class EventCreate(CreateView):
    model = Event
    form_class = EventForm

    def form_valid(self, form):
        form.instance.changed_by = self.request.user.username
        return super(EventCreate, self).form_valid(form)

    def get_initial(self):
        if not 'start' in self.kwargs or not self.kwargs['start']:
            return {}
        d = "%(d)s.%(m)s.%(y)s" % {
            'd':self.kwargs['start'].split('-')[2],
            'm':self.kwargs['start'].split('-')[1],
            'y':self.kwargs['start'].split('-')[0]
        }
        return { 'start': d, 'end': d }

class EventUpdate(UpdateView):
    model = Event
    form_class = EventForm

    def form_valid(self, form):
        form.instance.changed_by = self.request.user.username
        return super(EventUpdate, self).form_valid(form)


class EventItemList(ListView):
    model = EventItem
    # form_class = EventForm

    def get_queryset(self):
        self.event = get_object_or_404(Event, pk=self.kwargs['pk'])
        return EventItem.objects.filter(event=self.event)

    def get_context_data(self, **kwargs):
        kwargs = super(EventItemList, self).get_context_data(**kwargs)

        # print(self.__dict__)
        # event = Event.objects.get(pk=self.kwargs['pk'])

        # print('ahoj')
        # print(self.event.items)
        kwargs.update({
            "event": self.event,
            "stock_items": Item.objects.all()
        })

        return kwargs




class EventItemListCreateView(generics.ListCreateAPIView):
    serializer_class = EventItemSerializer

    def get_queryset(self):
        self.event = get_object_or_404(Event, pk=self.kwargs['event_pk'])
        return EventItem.objects.filter(event=self.event)

    def post(self, request, **kwargs):
        request.DATA['event'] = self.kwargs['event_pk']
        request.DATA['item'] = request.DATA['item_id']
        serializer = EventItemSerializer(data = request.DATA, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class EventItemRetrieveUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = EventItemSimpleSerializer
    queryset = EventItem.objects.all()


# #TODO procistit
# from rest_framework import permissions, viewsets

# # from .models import EventItem
# from .permissions import CanEditItems
# from .serializers import EventItemSerializer

# class EventItemViewSet(viewsets.ModelViewSet):
#     queryset = EventItem.objects.all()
#     serializer_class = EventItemSerializer

#     def get_permissions(self):
#         if self.request.method in permissions.SAFE_METHODS:
#             return (permissions.AllowAny(), )
#         return (permissions.IsAuthenticated(), CanEditItems(), )

#     def perform_create(self, serializer):
#         instance = serializer.save(author=self.request.user)

#         return super(EventItemViewSet, self).perform_create(serializer)

# class PostViewSet(viewsets.ModelViewSet):
#     queryset = Post.objects.order_by('-created_at')
#     serializer_class = PostSerializer

#     def get_permissions(self):
#         if self.request.method in permissions.SAFE_METHODS:
#             return (permissions.AllowAny(), )
#         return (permissions.IsAuthenticated(), IsAuthorOfPost(), )

#     def perform_create(self, serializer):
#         instance = serializer.save(author=self.request.user)

#         return super(PostViewSet, self).perform_create(serializer)
