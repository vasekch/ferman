
Ferman documentation!
=====================

This project is designed to help planning events (mostly cultural) and managing equipment inventory.

Case study is a company providing ground support, lights and sound for various concerts.

- Calendar can help organize events in time
- Equipment list helps to sort out paralel events concurency in advance
- Contact list can provide help when short on resources

Item lists are to be extended as PDF offer sheet generator and can provide dependency and weight advices for support.


Contents:

.. .. toctree::
..    :maxdepth: 2
.. * :ref:`genindex`
.. .. * :ref:`modindex`
.. * :ref:`search`


.. toctree::
    :maxdepth: 2

    install
    server_install
    develop

