.. server_install:

CentOS 6 Installation
=====================

CentOS 6 installation manual for production environment.

Database - SQLite
^^^^^^^^^^^^^^^^^

If SQLite can not provide sufficient performance for searching (items / contacts / items to event ... ) exchange it for PostgreSQL with proper indexes

Requirements
^^^^^^^^^^^^

install dependencies::

    yum install python33 git

Project specifics
^^^^^^^^^^^^^^^^^

Checkout project
----------------

make OS user ``ferman``::

    useradd -m ferman
    mkdir /opt/ferman
    chown ferman:ferman /opt/ferman/

switch to user ferman and check out using git::

    su - ferman
    cd /opt
    git clone https://bitbucket.org/vasekch/ferman.git ./ferman

pyvenv-3.4 env
--------------

Create virtual environment to separate project from system::

    cd /opt/ferman

    pyvenv env # python 3.3, use `pyvenv --without-pip env` for python 3.4
    source ./env/bin/activate

    wget https://pypi.python.org/packages/source/s/setuptools/setuptools-6.1.tar.gz
    tar -vzxf setuptools-6.1.tar.gz
    cd setuptools-6.1/
    python setup.py install
    cd ..

    wget https://pypi.python.org/packages/source/p/pip/pip-1.5.6.tar.gz
    tar -vzxf pip-1.5.6.tar.gz
    cd pip-1.5.6
    python setup.py install
    cd ..

    rm pip-1.5.6 setuptools-6.1 -rf
    rm pip-1.5.6.tar.gz setuptools-6.1.tar.gz

    deactivate
    source ./env/bin/activate

install requirements::

    pip install -r requirements.txt
    pip install gunicorn


configure
---------

change directory to src::

    cd src

create copy of settings template::

    cp settings/local.py.template settings/local.py

edit ``src/settings/local.py`` and set your credentials for database, etc. see all defaults ins ``src/settings/base.py`` and don't forget to add path to static media used by collectstatic::

    STATIC_ROOT = '/opt/ferman/static'

prepare database structure::

    ./manage.py migrate

prepare static media to /opt/ferman/static ::

    ./manage.py collectstatic

[optional] create superuser if you don't have one in your inital data or dump::

    ./manage.py createsuperuser


exit ferman user

    exit

Supervisord
^^^^^^^^^^^

Install supervisor as `root user` to system, not to virtualenv created for project::

    yum install python-pip
    pip install supervisord

Download init script and place it into /etc/init.d/supervisord

    https://github.com/Supervisor/initscripts/blob/master/redhat-init-jkoppe

Create /etc/supervisor/supervisord.conf::

    [unix_http_server]
    file = /var/run/supervisor.sock   ; (the path to the socket file)
    chmod = 0700                       ; sockef file mode (default 0700)
    chown = ferman:ferman

    [supervisord]
    logfile=/var/log/supervisor/supervisord.log ; (main log file;default $CWD/supervisord.log)
    pidfile=/var/run/supervisord.pid ; (supervisord pidfile;default supervisord.pid)
    childlogdir=/var/log/supervisor  ; ('AUTO' child log dir, default $TEMP)
    environment=LANG="en_US.utf8",LC_ALL="en_US.UTF-8",LC_LANG="en_US.UTF-8"

    ; the below section must remain in the config file for RPC
    ; (supervisorctl/web interface) to work, additional interfaces may be
    ; added by defining them in separate rpcinterface: sections

    [rpcinterface:supervisor]
    supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface

    [supervisorctl]
    serverurl = unix:///var/run/supervisor.sock ; use a unix:// URL  for a unix socket

    ; The [include] section can just contain the "files" setting.  This
    ; setting can list multiple files (separated by whitespace or
    ; newlines).  It can also contain wildcards.  The filenames are
    ; interpreted as relative to this file.  Included files cannot
    ; include files themselves.

    [include]
    files = /etc/supervisor/conf.d/*.conf


Create /etc/supervisor/conf.d/ferman.conf::

    [program:ferman_gunicorn]
    command=/opt/ferman/env/bin/gunicorn --workers=4 --timeout=1800 --bind=unix:/opt/ferman/gunicorn.sock wsgi
    directory=/opt/ferman/src/
    user=ferman
    autostart=true
    autorestart=true
    redirect_stderr=true

    [group:ferman]
    programs=ferman_gunicorn

Create symlink to standard path where supervisorctl expects configuration::

    ln -s /etc/supervisor/supervisord.conf /etc/

Restart supervisor service using supervisorctl command or standard way::

    /etc/init.d/supervisord restart

Check everything is ok in supervisorctl::

    supervisorctl
    supervisor> status

Nginx
^^^^^

Install nginx::

    yum install nginx

Edit proxy settings in ``/etc/nginx/proxy.conf``::

    proxy_redirect     off;
    proxy_set_header   Host             $host;
    proxy_set_header   X-Real-IP        $remote_addr;
    proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
    proxy_max_temp_file_size 0;
    proxy_connect_timeout      90;
    proxy_send_timeout         90;
    proxy_read_timeout         90;
    proxy_buffer_size          4k;
    proxy_buffers              4 32k;
    proxy_busy_buffers_size    64k;
    proxy_temp_file_write_size 64k;

Create ``/etc/nginx/sites-available/ferman`` or similar (in nginx 1.0) put into conf.d::

    server {
        listen 80;
        server_name ferman.rtdn.cz;
        access_log /opt/ferman/logs/nginx.access.log;
        client_max_body_size 10m;
        location / {
            proxy_pass http://unix:/opt/ferman/gunicorn.sock;
            include /etc/nginx/proxy.conf;
        }

        # ./manage.py collatestatic product
        location /static/ {
            expires 1m;
            alias /opt/ferman/static/;
        }

        # user media
        location /media/ {
            expires 1m;
            alias /opt/ferman/src/media/;
        }
    }

Enable Ferman in nginx::

    ln -s /etc/nginx/sites-available/ferman /etc/nginx/sites-enabled/

Restart nginx configuration::

    /etc/init.d/nginx restart

Services auto start
^^^^^^^^^^^^^^^^^^^

Add services to be automatically started after reboot::

    chkconfig nginx on
    chkconfig supervisord on


